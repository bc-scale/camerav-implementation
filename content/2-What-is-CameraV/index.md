# 1. What is CameraV?
---
CameraV is an Android application designed by the Guardian Project and Witness. CameraV was developed for use by activists, journalists, advocates and others, working in very di cult and high-risk situations, to capture and gather visual evidence and proof of abuse and rights violations. It can also be used in your daily life for documenting your own evidence, after accidents, for real estate needs, or anywhere you need documentation with additional proof that the material is unaltered.

![first image green table](/images/)

CameraV may be used in many circumstances, let’s consider how CameraV might improve the results in the following scenario.

## WITHOUT CameraV

Bob lives in a community that experiences regular harassment by the police. He sees an arrest in his neighborhood and records a video on his phone. During the arrest, police use excessive force against the suspect. When Bob submits the video as evidence of police abuses, the police claim Bob altered the video and did not record the incident fully. They claim the suspect threatened police prior to the use of force, justifying their response. Bob’s claim is not taken seriously and no change is made.

## WITH CameraV

Bob lives in a community that experiences regular harassment by the police. His friend Alice is a member of a local Cop Watch organization that trains community members to observe the police safely and effectively. Alice helps Bob install CameraV on his phone. When Bob sees an arrest in his neighborhood he records a video using the CameraV app on his phone. During the arrest, police use excessive force against the suspect. When Bob submits the video as evidence of police abuses, the police claim Bob altered the video and did not record the incident fully. They claim the suspect threatened police prior to the use of force, justifying their response. However, because he used CameraV, Bob is able to submit not only the video, but metadata that proves the video was unaltered. Bob’s claims are substantiated based on the additional proof provided by CameraV.

When a user runs CameraV for the  rst time, a setup wizard guides the user through the steps to create a passphrase for the application and to generate an encryption key based on the unique properties of the camera sensor. Every camera has a unique pattern of something called sensor noise. This is unique digital noise that is di erent for each sensor. This noise provides a “ ngerprint” of the device’s camera. By taking six boring photos CameraV can evaluate your sensor to generate this unique encryption key from your camera’s  ngerprint. This key will be used to verify that any piece of media captured using CameraV and uploaded to the secure server came from this speci c camera.

Once the setup is complete, the user has a choice of taking a picture or shooting a video. The CameraV sensors automatically activate when you begin recording. You have the option to enable Location, Bluetooth and WiFi services in your device’s settings to allow the app to use those sensors. If these services are not turned on, CameraV will not be able to gather data from their associated sensors. While shooting, CameraV continuously accesses the mobile device’s various sensors.

![second image green table](/images/)

This sensor data creates a digital snapshot of the environment where the media was captured and allows for later analysis to confirm the media was not only captured by a specific device on a specific date, time, and place, but enables cross-checking of this information against weather conditions at the time. It will also record what WiFi networks and Bluetooth devices were in the vicinity so that these can be used for corroboration, and in the case of mobile phones what cell towers the device was communicating with.

The enhanced metadata is bundled within the media  le, encrypted, and given a unique digital signature, also known as a hash. This digital signature is short enough to quickly be sent via email or SMS text to a secure server or other 3rd party and used to verify that neither the media nor the metadata was altered in any way after it was captured. This can be very useful in cases where the user has limited access to a high speed data connection.

For example, if a user is recording footage in a rural area of Kenya that has limited cellular network coverage but no data service, CameraV can send the hash of the media file via SMS to a contact in a safe environment. This provides a notarization of the  le that the user can use when CameraV completes the upload process to verify the media file is authentic and has not been tampered with in the intervening period.

In Chapter 2 you’ll be able to follow a walkthrough explaining how to use CameraV. If you’ve already been experimenting with the tool you may want to skip ahead to Chapter 3, which gets into the specific features CameraV uses to increase the veracity of your media.
