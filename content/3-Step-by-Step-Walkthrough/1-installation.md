# INSTALLATION

Before you can use the CameraV app, you must install it. You can find download links for the application at the following web URL: https://guardianproject.info/apps/camerav or search Google Play for “CameraV”.

FIRST TIME SETUP
The first time you use CameraV, you must configure it with basic identity information. The app asks for an identifying name, which can be your real name or a pseudonym. You may also include an email address which may be your standard email or a special one you set up just for use with CameraV. Your email will be included with the metadata, ensuring that someone who may later find the photo or video you captured can contact you.

![first image](/images/)

You must also enter and con rm a passphrase to use CameraV. This passphrase is used to protect the photos, video, and sensor metadata stored within the app itself. It is the secret combination for the vault of data the app is protecting.
Finally, you must take six sample photos with CameraV during the setup process. This generates a visual fingerprint of your camera sensor that can be used later to prove that a specific image or video did or did not come from your camera. The app will ask you to take a “boring” photo, which means a blank wall, table or some other uncomplicated surface. The need for “boring” is a technical detail related to the visual fingerprint process.

![second image](/images/)

Once you complete all of these steps the setup process is finished and you will be taken to the CameraV homescreen.
