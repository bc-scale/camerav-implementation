# USING THE CAMERA

![first image](images/)

From the Home View, you can launch either the photo or video camera. By default, the app will launch the built-in external camera app that is available on your phone. It will monitor the external camera app, and when new photos or videos are captured, it will inspect them, extract their metadata, and store a thumbnail and the data in the internal encrypted storage. While they will show up in the Gallery view, the actual media file are stored unencrypted in the standard device media storage location on the external memory or microSD card.

![second image](/images)

To use the encrypted camera capabilities of CameraV, you must go into the CameraV Settings, and uncheck the “Use External Camera Apps” option. Then the camera action buttons on the Home view will launch the built-in encrypted camera feature. This provides a basic still and video camera capability, with the resulting photo or video file written directly to encrypted storage. This means that only CameraV can see the media and if you delete a  file it is completely deleted from the device, and not recoverable.

![third image](/images/)

The encrypted camera has only a button to take a picture or to start and stop video recording. All other settings are automatic.

When you are done capturing media, you can use the back key to return to the Home View or the Gallery View to see the captured media files.

![fourth image](/images/)
