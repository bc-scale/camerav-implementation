# HOW DO I NOTARIZE MEDIA?

When you choose the ‘Notarize’ option, a short message is generated that includes the unique identifier, or “fingerprint”, of the media file. This message can be shared with any of the apps on your device that allow you to share messages, like SMS, email, Twitter, Facebook or other apps of that sort.

[first image](/images/)

## PUBLIC VERSUS PRIVATE NOTARIZATION

You can choose to publish the notarization message publicly, which will create a public timestamp that this media existed in this specific state.

You can also choose to share the notarization message privately, which can be used by the recipient to compare the media or metadata when they receive the file at a later date.

If you are still unsure why to use these features the brief scenario that follows should help encapsulate some uses for sharing and notarizing your metadata without the media.

Let’s look at how notarization benefits another user of CameraV.

Sarah is a human rights defender who is gathering testimony from witnesses to describe a human rights violation after it happened. She used CameraV and recorded each question as a different video clip. She used the notarization feature, encrypted the video files and uploaded them over WiFi to the Internet. She received a notification from her colleague that it arrived intact, after which she deleted the copies on her phone. After processing, the material was subsequently submitted to a human rights lawyer.

Researchers for the lawyer previously reviewed testimony alleged to be from one of the individuals Sarah interviewed. These testimonies included significant variations from Sarah’s video, and each other. Because Sarah’s material contained metadata that was verifiable through CameraV, Sarah’s data was a major lead in the lawyer’s investigation.

In this case, the content of the video was not the only important piece of data. The fact that these video clips were verifiable, and had metadata that said something about the files made the data contained in the videos valuable.

[second image](/images/)
