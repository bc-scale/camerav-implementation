# WHAT IF I LOSE MY PHONE?

Loss and theft are likely the highest risk facing your smartphone and the data it contains. Fortunately, CameraV supports locking access to itself and data behind a passphrase. When you  rst open CameraV after starting the device, you will be required to enter the passphrase you chose on setup. The app should remain unlocked until you manually lock the app, or restart your device.

[first image](/images/)
