# DELETE FILES QUICKLY

## WHAT CAN I DO IF I DID NOT PREPARE MYSELF AND NEED TO DELETE FILES QUICKLY BEFORE I AM DETAINED BY A POLICE OFFICER OR OTHER MEMBERS OF A SECURITY SERVICE?

As a human rights defender, you have a responsibility to yourself and the witnesses you interview to take every possible precaution to protect them and yourself. You should always evaluate the risk to yourself and those you will meet before you engage in documentation, and have your evaluation and contingency plan reviewed by an experienced peer or superior.

If, despite every effort to maintain safety, you find yourself at risk of detention and exposing sensitive material, there is an option to delete the media from your device. At the top of the home screen you will see the icon of an open lock. Press this icon and you will see two options: Lock App and Panic. If you select “Lock App”, CameraV will immediately close and require your passphrase to open the application and see your encrypted media.

If you select “Panic” you will see an icon for radioactive waste appear on the screen. If you swipe this icon to the bottom of the screen, CameraV will immediately delete all of your media and associated metadata, as well as your passphrase. After this action, CameraV will appear in your application tray as a new app that you have never before opened.

![first image](/images/)

You may want the application to be deleted entirely. To do this, you must access the CameraV settings menu, via the menu button on the home screen. Under “Panic Action” you will see the action “Delete my content” set by default. If you change this to “Delete entire app” the Panic action will delete not only your media and associated metadata, but the CameraV app as well.
