# PROVING THE MEDIA

## HOW DO I PROVE THE MEDIA AND METADATA COME FROM ME AND MY DEVICE?

CameraV includes signing and encryption of data by default. All metadata is signed by the private key of the user that is generated during the app setup. This signature data is included in the J3M document when you export it.
