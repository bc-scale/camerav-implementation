# CameraV PERMISSIONS

## WHY DOES CameraV NEED SO MANY APPLICATION PERMISSIONS?

CameraV requires access to a large set of permissions in order to access all the sensor and device metadata required. Below is the list of the primary required permissions and a short explanation why they are needed.
- Full network access: Allows the app to make network connections of any sort, including access to remote servers.
- Approximate and Precise Location: Allows the app to determine your location through a variety of means including using the GPS and Network-based location features.
- Read Phone Status and Identity: Allows the app to access your device identifier, this helps create the unique identifier associated with your media files. This is part of the proof that it really came from your device. However, this data is never publicly shared, so your real identity is only shared if you choose to.
- Retrieve Running Apps: Allows the app to retrieve information about currently and recently running apps. This is used as a countermeasure against third-party apps that may fake metadata sensor information.

- Access Bluetooth Settings: Allows the app to activate and manage your Bluetooth connection. This is used to gather data about Bluetooth devices in your area, and combine that with the sensor metadata.
- Connect/Disconnect from the Wireless LAN (WLAN): Allows the app to manage WiFi and WLAN connections, which like with Bluetooth, is used to gather data about the networks in your immediate vicinity, as part of the sensor metadata.
- Find Accounts on Device: Allows the app to access the list of accounts known by the device. This is used by the app if you want to share to Google Drive, so CameraV knows which Google accounts are available and active.
- Modify or Delete Contents of the SD Card: Allows the app to read and write data to SD Card external storage. Only media or files related to CameraV are accessed or removed.
- Record Audio / Take Pictures and Videos: Allows the app to record audio and capture pictures or video. Without this permission CameraV cannot capture media itself and must depend on other camera apps on the device.
