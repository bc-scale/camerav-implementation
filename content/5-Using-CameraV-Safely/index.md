# 4. USING CameraV SAFELY AS A HUMAN RIGHTS DEFENDER
---
In Chapter 4 we will look at some scenarios you may encounter while using CameraV, and how certain features of CameraV will help you use the app responsibly. To use CameraV responsibly you must understand how to protect sensitive materials and deliver them only to the proper individual or individuals. The metadata gathered by CameraV provides a great deal of unique information that may put individuals or entire communities at risk if it is obtained by individuals who intend to do harm.

![first image](/images/)
