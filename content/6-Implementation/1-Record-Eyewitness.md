# RECORD EYEWITNESS TESTIMONY ON VIDEO?

In many war crimes events, evidence is gathered after the fact through testimonial interviews with victims, witnesses, or others with knowledge of the incident. It is of prime importance that these interviews are protected and only accessible to those who need access. You should only use CameraV for recording this type of video because CameraV will keep the content in an encrypted container. The encrypted container prevents other apps on your device from accessing the video, and ensures the content cannot be viewed without your device and specific CameraV passphrase. See Chapter 4 for more information about this and other security features in CameraV.

![first image](/images/)

If an individual (known as a source) contacts you to report on a crime they witnessed, you should follow these steps to increase the efficacy of the video testimony and the likelihood it can be used as evidence:

1. Gather information about your source, ensure your research supports their claim.
2. Be sure you have a list of relevant questions before you meet the source.
3. Establish an emergency contact who will not go with you and should be safe during the time you are recording your testimony. This individual should have good phone and Internet connectivity to ensure they receive your SMS message and media files. If you have time, send them a message first to ensure they are ready to receive your  les.
4. Make a plan for storing and distributing the material before you meet to record their testimony.
a. If you must travel to a remote area, assess whether there are police checkpoints or other potential risks to the security of the testimony along your route.
i. Especially if such risks exist, you should add an additional passphrase to lock your device.
ii. Turn your device o  once you have completed recording.
5. Ensure you have enough storage on your device to capture the entirety of the source’s testimony and supporting images.
6. Ensure you have contacts who can act on the material, or a plan for what to do with the testimony after it is recorded.
7. Once the recording is finished, if you may lose your device, share the file and associated metadata with a trusted colleague away from danger.
8. Once you are safe, make a backup copy of the testimony and associated metadata and encrypt it with PGP.
9. Delete the files from your device.
10. If your subject desires, be sure to keep them apprised of the status of their case, as long as it will not put them or anyone else in danger.

![second image](/images/)
