# DOCUMENT A PROTEST, MARCH, OR PUBLIC GATHERING?

It is a common sight to see hundreds of phones emerging out of the crowd, documenting whatever they can see, during any large public gathering. While you might not need the metadata provided by CameraV, using CameraV for your video and photo documentation will ensure that you have that added layer of veracity, should something unexpected occur.
These steps will help you prepare to document public events effectively:
1. Find at least one person to go with you, this person should not do documentation, but only keep an eye on the situation and help you know if you may be in danger.
2. Create a list of questions you want to ask, and a list of visuals to capture.
3. Establish an emergency contact who will not go with you and should be safe during the time you are recording the event. This individual should have good phone and Internet connectivity.
4. If there will be individuals or an organization providing legal support, at least keep their contact information accessible. If possible, inform them you will be documenting events, in case they need specific images later.
5. Ensure you have enough storage on your device to capture the entirety of your list of visuals and desired number of interviews.
6. If you capture something that may be valuable as evidence, share the file and associated metadata with a trusted colleague away from danger, in case your device becomes lost, stolen, or confiscated.
7. Once you are safe, make a backup copy of the testimony and associated metadata and encrypt it with PGP.
8. Delete the files from your device.
9. If you captured something that may be valuable as evidence, be sure to contact the legal support affliated with the event.

![second image](/images/)
