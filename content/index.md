# Introduction
---
There is a deluge of photos and video coming from the world’s mobile devices for potential use as evidence or trusted sources for journalists and human rights defenders. WITNESS and the Guardian Project created CameraV to verify and authenticate this footage.

CameraV uses smartphones’ built-in sensors for tracking movement, light and other environmental inputs, along with WiFi, Bluetooth, and cellular network metadata to capture a snapshot of the environment while you are taking a photo or video. This extra metadata (the data about the data!) helps verify and validate the date, time and location of capture, and provides an entirely new layer of context and meaning. Digital signatures and encryption ensure your media can only be seen by the people you choose and hasn’t been tampered with after it was captured.

Pictures taken with mobile phones often contain embedded information, known as metadata, that provide information such as the type of device that took the picture and the date and time. They may also contain GPS coordinates that can be used to determine where an image or video was captured. Sometimes metadata exposes information the user doesn’t want shared.

However, there are times when citizen journalists and human rights defenders might need to preserve more information rather than less, to ensure their digital files can be used as legal evidence or archived properly. These individuals may want to have geolocation metadata, and the ability to protect the metadata from tampering, to increase the chances that legal institutions or news outlets trust the content. In an age where digital manipulation of images and video is commonplace, news agencies and legal institutions have to contend with the possibility that digitally altered media is being passed off as unadulterated truth.

CameraV is well suited for capturing photographic evidence of an incident of any nature. When capturing evidence the goal is to capture the state of some thing, at a speci c point of time, and then share that media evidence with an authority who can use it. CameraV will improve the veracity of your images and help you  ght accusations of manipulation.

A secondary goal may be for advocacy and awareness. Using CameraV helps bolster your case, to provide proof to your broader public audience that what they are seeing is true, authentic, and unmodified.

#### Read on to find out more!
